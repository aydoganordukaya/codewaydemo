//
//  BaseViewController.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 7.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import UIKit

class BaseViewController:UIViewController{
    
    let scaleTransitionAnimator = ScaleTransitionAnimator()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var prefersStatusBarHidden: Bool { return true }

    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Creation
    class var storyboardName: String {
        return String(describing: self)
    }
    
    class func createFromStoryboard() -> Self {
        let vc = createFromStoryboard(named: storyboardName, type: self)
        if let vc = vc as? UIViewController {
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
        }
        return vc
    }
    
    static func createFromStoryboard<T: UIViewController>(named storyboardName: String?, type: T.Type) -> T {
        let vc = UIStoryboard(name: storyboardName ?? self.storyboardName, bundle: Bundle.main).instantiateInitialViewController() as! T
        if let vc = vc as? UIViewController {
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .fullScreen
        }
        return vc
    }
}
