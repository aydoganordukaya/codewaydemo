//
//  ContentProgressView.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 9.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import UIKit


protocol ContentProgressViewDataSource {
    func numberOfContent(_ contentProgressView: ContentProgressView) -> Int
    func contentProgressView(_ contentProgressView: ContentProgressView, contentDurationAt index:Int) -> Int
    func contentProgressView(_ contentProgressView: ContentProgressView, isShowedAt index:Int) -> Bool
}

protocol ContentProgressViewDelegate {
    func contentProgressView(_ contentProgressView: ContentProgressView,beginContentAt index:Int)
    func startNextStory(for contentProgressView: ContentProgressView)
    func startPreviousStory(for contentProgressView: ContentProgressView)

}

class ContentProgressView:UIView{
    var dataSource:ContentProgressViewDataSource?
    var delegate:ContentProgressViewDelegate?
    
    var fillerViews = [UIView]()
    var fillerLayers = [CAShapeLayer]()
    
    private let padding:CGFloat = 1.0
    private var currentIndex:Int = 0{
        didSet{
            if oldValue != currentIndex{
                self.startAnimate(at: currentIndex)
            }
            self.delegate?.contentProgressView(self, beginContentAt: currentIndex)
        }
    }
    private var numberOfContent:Int?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }

    func arrangeView(){
        if let c = self.dataSource?.numberOfContent(self), c > 0{
            self.numberOfContent = c
        }
        layer.cornerRadius = self.frame.width
        self.drawBaseProgress()
    }

    private func drawBaseProgress(){
        guard let contentCount = self.numberOfContent else { return }
        let width = (self.frame.size.width - (CGFloat(contentCount - 1) * padding)) / CGFloat(contentCount)
        let height = self.frame.size.height
        var currentPoint:CGPoint = .zero
        let baseSize = CGSize(width: width, height: height)

        fillerLayers.forEach { (l) in
            l.removeAllAnimations()
        }
        fillerLayers.removeAll()
        self.layer.sublayers?.removeAll()
        var animatedIndex:Int?
        for i in 0..<contentCount{
            let basePath = UIBezierPath()
            basePath.move(to: currentPoint)
            basePath.addLine(to: CGPoint(x: currentPoint.x + width, y: currentPoint.y))
            
            let baseLayer = CAShapeLayer()
            baseLayer.bounds = CGRect(origin: currentPoint, size: baseSize)
            baseLayer.frame = CGRect(origin: currentPoint, size: baseSize)
            baseLayer.path = basePath.cgPath
            baseLayer.lineWidth = height
            baseLayer.cornerRadius = height
            baseLayer.strokeColor = UIColor.white.withAlphaComponent(0.5).cgColor
            self.layer.addSublayer(baseLayer)
            
            
            let fillerLayer = CAShapeLayer()
            fillerLayer.bounds = CGRect(origin: currentPoint, size: baseSize)
            fillerLayer.frame = CGRect(origin: currentPoint, size: baseSize)
            fillerLayer.lineWidth = height
            fillerLayer.cornerRadius = height
            fillerLayer.path = nil
            fillerLayer.strokeColor = UIColor.white.cgColor
            if self.dataSource?.contentProgressView(self, isShowedAt: i) ?? false{
                animatedIndex = i
                let path = UIBezierPath()
                path.move(to: currentPoint)
                path.addLine(to: CGPoint(x:  currentPoint.x + width, y: currentPoint.y))
                fillerLayer.path = path.cgPath
            }
            
            
            self.fillerLayers.append(fillerLayer)
            self.layer.addSublayer(fillerLayer)

            currentPoint = CGPoint(x: currentPoint.x + padding + width, y: currentPoint.y)

        }
        if let i = animatedIndex{
            startAnimate(at: i + 1)
        }else{
            startAnimate(at: 0)
        }
        
    }
    
    func startAnimate(at index:Int){
        guard let contentCount = self.numberOfContent else { return }
        guard let duration = self.dataSource?.contentProgressView(self, contentDurationAt: index) else { return }
        
        if index < self.fillerLayers.count{
            let layer = self.fillerLayers[index]
            CATransaction.begin()
            CATransaction.setCompletionBlock({
                self.startNextContent()
            })
            let width = (self.frame.size.width - (CGFloat(contentCount - 1) * padding)) / CGFloat(contentCount)
            
            let path = UIBezierPath()
            path.move(to: layer.frame.origin)
            path.addLine(to: CGPoint(x:  layer.frame.origin.x + width, y: layer.frame.origin.y))
            layer.path = path.cgPath
            
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.fromValue = 0
            animation.toValue = 1
            animation.duration = CFTimeInterval(duration)
            layer.add(animation, forKey: "line")
            CATransaction.commit()
        }
    }
    
    func removeFiller(at index:Int){
        guard self.numberOfContent != nil else { return }
        if index < self.fillerViews.count{
            let layer = self.fillerLayers[index]
            layer.removeAllAnimations()
        }
    }
    
    func startNextContent(){
        guard let contentCount = self.numberOfContent else { return }
        self.removeFiller(at: currentIndex)
        if currentIndex < contentCount - 1{
            currentIndex += 1
        }else if currentIndex == contentCount - 1{
            self.delegate?.startNextStory(for: self)
        }
    }
    
    func startPreviouseContent(){
        if currentIndex > 0{
            self.removeFiller(at: currentIndex)
            self.removeFiller(at: currentIndex - 1)

            currentIndex -= 1
        }else if currentIndex == 0{
            self.removeFiller(at: 0)
            self.delegate?.startPreviousStory(for: self)
        }
    }
    
    func startContent(at index:Int){
        guard let contentCount = self.numberOfContent else { return }
        if index > 0, index < contentCount - 1{
            for i in min(currentIndex, index)...max(currentIndex, index){
                self.removeFiller(at: i)
            }
            currentIndex = index
        }
    }

}
