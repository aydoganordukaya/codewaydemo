//
//  CALayerExtension.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 10.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import UIKit

extension CALayer
{
    func pauseAnimation() {
        if isPaused() == false {
            let pausedTime = convertTime(CACurrentMediaTime(), from: nil)
            speed = 0.0
            timeOffset = pausedTime
        }
    }

    func resumeAnimation() {
        if isPaused() {
            let pausedTime = timeOffset
            speed = 1.0
            timeOffset = 0.0
            beginTime = 0.0
            let timeSincePause = convertTime(CACurrentMediaTime(), from: nil) - pausedTime
            beginTime = timeSincePause
        }
    }

    func isPaused() -> Bool {
        return speed == 0
    }
    
    func finishAnimation(){
        speed = .infinity
    }
    
}

