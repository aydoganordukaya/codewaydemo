//
//  CustomLayout.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 7.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import UIKit


class CubicLayout:UICollectionViewFlowLayout{
    
    let angle: CGFloat = .pi / 2 // 90 degree

    
    override func prepare() {
        super.prepare()
        guard let collectionView = collectionView else { return }
        
        let itemCount = collectionView.numberOfItems(inSection: 0)
        let width = CGFloat(itemCount) * collectionView.frame.size.width
        let height = collectionView.frame.size.height
        
        contentSize = CGSize(width: width, height: height)
    }
    
    open class CubicLayoutAttributes: UICollectionViewLayoutAttributes{
        var contentView: UIView?
        /// if angleRatio == 0  only one visible cell
        /// if angleRatio *<* 0 left cell
        /// if angleRati0 *>*  0  right cell
        var angleRation: CGFloat = 0
    }
    var contentSize:CGSize = .zero
    override var collectionViewContentSize: CGSize{
        return contentSize
    }
    
    override class var layoutAttributesClass: AnyClass { return CubicLayoutAttributes.self }

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    open override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) else { return nil }
        
        let cubicAttrs = attributes.compactMap { (attr) -> CubicLayoutAttributes? in
            if var cubicAttr = attr.copy() as? CubicLayoutAttributes{
                self.arrangeTransform3D(attribute: &cubicAttr)
                return cubicAttr
            }
            return nil
        }
        return cubicAttrs
    }
    
    
    func arrangeTransform3D(attribute: inout CubicLayoutAttributes) {
        guard let collectionView = self.collectionView else { return }
        
        let dif = attribute.frame.origin.x - collectionView.contentOffset.x
        
    
        attribute.angleRation = dif / collectionView.frame.width
        
        if attribute.contentView == nil{
            attribute.contentView = collectionView.cellForItem(at: attribute.indexPath)?.contentView
        }
        // angleRatio > 1 or < -1 , it means this cells are at least 2 unit away and not visible yet, we need to set transform and anchors to default for reuse cell
        if abs(attribute.angleRation) >= 1{
            attribute.contentView?.layer.transform = CATransform3DIdentity
            attribute.contentView?.setAnchorPoint(CGPoint(x: 0.5, y: 0.5))
        }else{
            let rotateAngle = angle * attribute.angleRation
            var transform = CATransform3DIdentity
            transform.m34 = -1 / 500
            transform = CATransform3DRotate(transform, rotateAngle, 0, 1, 0)
            attribute.contentView?.layer.transform = transform
            
            // angleRatio > 0, that means the cell on the right side's left most x point needs to be anchored
            // angleRatio < 0, that means the cell on the left side's right most x point needs to be anchored
            attribute.contentView?.setAnchorPoint(CGPoint(x: attribute.angleRation > 0 ? 0 : 1, y: 0.5))
        }
    }
    
}
