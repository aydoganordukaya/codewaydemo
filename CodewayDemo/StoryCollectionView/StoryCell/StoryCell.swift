//
//  StoryCell.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 7.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import UIKit

protocol StoryCellDelegate {
    func startNextStroy()
    func startPreviouseStory()
}

class StoryCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var contentProgressView: ContentProgressView?
    var delegate:StoryCellDelegate?
    
    var story:Story?
    func arrange(story:inout Story){
        self.story = story
        self.contentProgressView?.dataSource = self
        self.contentProgressView?.delegate = self
        self.contentProgressView?.arrangeView()
        
        imageView?.largeContentImageInsets = .zero
 

        let index:Int = story.contents.firstIndex { (c) -> Bool in
            return !c.isShowed
        } ?? 0
        
        story.contents[index].isShowed = true
        imageView?.image = UIImage(named: story.contents[index].name)
        
    }
    
}

extension StoryCell:ContentProgressViewDataSource,ContentProgressViewDelegate{
    func numberOfContent(_ contentProgressView: ContentProgressView) -> Int {
        return self.story?.contents.count ?? 0
    }
    
    func contentProgressView(_ contentProgressView: ContentProgressView, contentDurationAt index: Int) -> Int {
        guard let contents = self.story?.contents else { return 0}
        if index >= contents.count{
            return 0
        }
        return contents[index].duration
    }
    
    func contentProgressView(_ contentProgressView: ContentProgressView, beginContentAt index: Int) {
        guard let contents = self.story?.contents else { return }
        if index > 0 {
            self.story?.contents[index - 1].isShowed = true
        }
        imageView?.image = UIImage(named: contents[index].name)
    }
    
    func contentProgressView(_ contentProgressView: ContentProgressView, isShowedAt index:Int) -> Bool
    {
        guard let contents = self.story?.contents else { return false}
        return contents[index].isShowed
    }
    
    func startNextStory(for contentProgressView: ContentProgressView) {
        self.delegate?.startNextStroy()
    }
    
    func startPreviousStory(for contentProgressView: ContentProgressView) {
        self.delegate?.startPreviouseStory()
    }
    
    
}
