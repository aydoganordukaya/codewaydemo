//  
//  StoryCollectionViewController.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 7.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import UIKit

final class StoryCollectionViewController: BaseViewController {

    @IBOutlet weak var panGesture:UIPanGestureRecognizer?
    @IBOutlet weak var collectionView:UICollectionView?
    private let animator = ScaleTransitionAnimator()
    
    var touchStartLocation:CGPoint?

    //MARK: - View Model
    
    var viewModel: StoryCollectionViewModelProtocol!
    
    //MARK: - View Life Cycle
    
    static func createFromStoryboard(transitionFrame: CGRect) -> StoryCollectionViewController {
        let viewController = createFromStoryboard()
        viewController.animator.originFrame = transitionFrame
        viewController.transitioningDelegate = viewController
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.contentInset = .zero
        bindViewModel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.generateStories()

    }
    //MARK: - Bind View Model
    
    private func bindViewModel() {
        viewModel.changeHandler = { [weak self] change in
            guard let self = self else { return }
            switch change {
                case .loaderStart:
                    //self.showLoader()
                    print("Show Loader")
                case .loaderEnd:
                    //self.dismissLoader()
                    print("Dismiss Loader")
                case .error(let message):
                    //self.showMessage(message)
                    print(message)
                case .storiesGenerated:
                    self.collectionView?.reloadData()
            }
        }
    }
    @IBAction func cellTapped(_ sender:UITapGestureRecognizer){
        let location = sender.location(in: self.view)
        guard let collectionView = collectionView, collectionView.indexPathsForVisibleItems.count > 0 else{ return}
        
        let currentItem = collectionView.indexPathsForVisibleItems[0]

        if location.x <= collectionView.frame.size.width / 5 && currentItem.row < self.viewModel.stories.count{
            
            if self.viewModel.stories[currentItem.row].contents.count == 1{
                let previousIndex = IndexPath(row: currentItem.row - 1, section: currentItem.section)
                collectionView.scrollToItem(at: previousIndex, at: .left, animated: true)
                collectionView.reloadItems(at: [currentItem,previousIndex])

            }else{
                var index = 0
                for (i,c) in self.viewModel.stories[currentItem.row].contents.enumerated(){
                    if c.isShowed{
                        index = i
                    }
                }
                if index == 0{
                    let previousIndex = IndexPath(row: currentItem.row - 1, section: currentItem.section)
                    collectionView.scrollToItem(at: previousIndex, at: .left, animated: true)
                    collectionView.reloadItems(at: [currentItem,previousIndex])

                }else{
                    self.viewModel.stories[currentItem.row].contents[index - 1].isShowed = false
                    self.viewModel.stories[currentItem.row].contents[index].isShowed = false
                    collectionView.reloadItems(at: [currentItem])
                }
            }
        }else if location.x > collectionView.frame.size.width / 5 && currentItem.row < self.viewModel.stories.count - 1{
            if self.viewModel.stories[currentItem.row].contents.count == 1{
                let nextIndex = IndexPath(row: currentItem.row + 1, section: currentItem.section)
                collectionView.scrollToItem(at: nextIndex, at: .right, animated: true)
                collectionView.reloadItems(at: [currentItem,nextIndex])

            }else{
                var index = 0
                for (i,c) in self.viewModel.stories[currentItem.row].contents.enumerated(){
                    if c.isShowed{
                        index = i
                    }
                }
                if index == (self.viewModel.stories[currentItem.row].contents.count - 1){
                    let nextIndex = IndexPath(row: currentItem.row + 1, section: currentItem.section)
                    collectionView.scrollToItem(at: nextIndex, at: .right, animated: true)
                    collectionView.reloadItems(at: [currentItem,nextIndex])
                }else{
                    collectionView.reloadItems(at: [currentItem])
                }
            }
            
        }
    }

    @IBAction func didSwipeDown(_ sender: UIPanGestureRecognizer) {
        if sender != panGesture{
            return
        }
       
        let currentLocation = sender.location(in: self.view)
        
        switch sender.state {

        case .began:
            touchStartLocation = sender.location(in: self.view)
        case .failed, .ended:
            touchStartLocation = nil
        case .cancelled:
            return
        default:
            break
        }
        print("current: \(currentLocation)")
        if let initalTouch = touchStartLocation{
            print("start: \(initalTouch)")
            if currentLocation.y - initalTouch.y >  150 {
                self.dismiss(animated: true, completion: nil)
            } else if currentLocation.y <= initalTouch.y{
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            } else {
                UIView.animate(withDuration: 0.1, animations: {
                    self.view.frame = CGRect(x: 0, y: currentLocation.y - initalTouch.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            })
        }
    }
}

extension StoryCollectionViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.stories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryCell", for: indexPath) as? StoryCell{
            cell.arrange(story: &self.viewModel.stories[indexPath.row])
            cell.clipsToBounds = true
            cell.delegate = self
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width , height: view.bounds.height)
    }
    
}

extension StoryCollectionViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.presenting = true
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.presenting = false
        return animator
    }

}
extension StoryCollectionViewController: UIGestureRecognizerDelegate{

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension StoryCollectionViewController:StoryCellDelegate{
    func startNextStroy() {
        guard let collectionView = collectionView, collectionView.indexPathsForVisibleItems.count > 0 else{ return}
        let currentItem = collectionView.indexPathsForVisibleItems[0]
        let nextIndex = IndexPath(row: currentItem.row + 1, section: currentItem.section)
        collectionView.scrollToItem(at: nextIndex, at: .right, animated: true)
        collectionView.reloadItems(at: [currentItem,nextIndex])
    }
    
    func startPreviouseStory() {
        guard let collectionView = collectionView, collectionView.indexPathsForVisibleItems.count > 0 else{ return}
        let currentItem = collectionView.indexPathsForVisibleItems[0]
        let previousIndex = IndexPath(row: currentItem.row - 1, section: currentItem.section)
        collectionView.scrollToItem(at: previousIndex, at: .left, animated: true)
        collectionView.reloadItems(at: [currentItem,previousIndex])
    }
    
    
}
