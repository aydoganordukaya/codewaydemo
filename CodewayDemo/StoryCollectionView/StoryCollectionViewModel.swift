//  
//  StoryCollectionViewModel.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 7.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import Foundation

struct Story {
    let name:String
    var contents:[Content]
}
struct Content {
    let name:String
    let duration:Int
    var isShowed:Bool = false
}


class StoryCollectionViewModel: StoryCollectionViewModelProtocol {

    var changeHandler: ((StoryCollectionViewModelChange) -> Void)?
    
    var stories = [Story]()
    
    func generateStories(){
        stories.removeAll()
        
        let storyCount = Int.random(in: 4...10)
        for _ in 0..<storyCount{
            let contentCount = Int.random(in:2...4)
            var contents = [Content]()
            for _ in 0..<contentCount{
                let contentName = "image\(Int.random(in: 0...6))"
                let duration = Int.random(in: 1...3)
                let content = Content(name: contentName, duration: duration)
                contents.append(content)
            }
            let name = randomString(length: 5)
            let story = Story(name: name, contents: contents)
            stories.append(story)
        }
        self.emit(.storiesGenerated)
    }
    
    private func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyz"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    
    //MARK: - Emit
    
    private func emit(_ change: StoryCollectionViewModelChange) {
        changeHandler?(change)
    }
}
