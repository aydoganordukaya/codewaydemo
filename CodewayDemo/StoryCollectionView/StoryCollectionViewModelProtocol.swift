//  
//  StoryCollectionViewModelProtocol.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 7.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import Foundation

enum StoryCollectionViewModelChange {
    case loaderStart
    case loaderEnd
    case error(message: String)
    case storiesGenerated
}

protocol StoryCollectionViewModelProtocol {
    var stories: [Story] { get set}
    
    func generateStories()
    
    var changeHandler: ((StoryCollectionViewModelChange) -> Void)? {get set}
}

