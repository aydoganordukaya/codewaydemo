//
//  ScaleTransitionAnimator.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 8.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//
import UIKit

class ScaleTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let durationExpanding = 0.75
    let durationClosing = 0.5
    var presenting = true
    var originFrame = CGRect.zero
    var originView: UIView?
    var dismissCompletion: (()->Void)?
    
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        if presenting {
            return durationExpanding
        }
        return durationClosing
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        var toView:UIView
        var detailView:UIView
        if let _toView = transitionContext.view(forKey: .to),  let _detailView = presenting ? _toView: transitionContext.view(forKey: .from){
            toView = _toView
            detailView = _detailView
        }else{
            if !presenting && transitionContext.view(forKey: .to) == nil && originFrame != .zero ,
                let _detailView = transitionContext.view(forKey: .from){
                toView = UIView(frame: originFrame)
                detailView = _detailView
            }else{
                transitionContext.completeTransition(true)
                return
            }
        }

        
        let initialFrame = presenting ? originFrame : detailView.frame
        let finalFrame = presenting ? detailView.frame : originFrame
        let xScaleFactor = presenting ? initialFrame.width / finalFrame.width : finalFrame.width / initialFrame.width
        let yScaleFactor = presenting ? initialFrame.height / finalFrame.height : finalFrame.height / initialFrame.height
        let scaleTransform = CGAffineTransform(scaleX: xScaleFactor,
                                               y: yScaleFactor)
        
        if presenting {
            detailView.transform = scaleTransform
            detailView.center = CGPoint( x: initialFrame.midX, y: initialFrame.midY)
            detailView.clipsToBounds = true
        }else{
            detailView.transform = CGAffineTransform.identity
            detailView.center = CGPoint( x: initialFrame.midX, y: initialFrame.midY)
            detailView.clipsToBounds = true

        }
        
        let toBgColor = UIColor.clear
        
        containerView.addSubview(toView)
        containerView.bringSubviewToFront(detailView)
        
        if presenting {
            toView.backgroundColor = originView?.backgroundColor
            UIView.animate(withDuration: durationExpanding, delay:0.0,
                           usingSpringWithDamping: 1.0, initialSpringVelocity: 0.3,
                animations: {
                    toView.backgroundColor = toBgColor
                    detailView.transform = CGAffineTransform.identity
                    detailView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
            },
                completion:{_ in
                    transitionContext.completeTransition(true)
            }
            )
        } else {
            
            UIView.animate(withDuration: durationClosing, delay:0.0,
                           usingSpringWithDamping: 1.0, initialSpringVelocity: 0.3,
                animations: {
                    detailView.alpha = 0
                    detailView.transform = scaleTransform
                    detailView.frame = finalFrame
            },
                completion:{_ in
                    if !self.presenting {
                        self.dismissCompletion?()
                    }
                    transitionContext.completeTransition(true)
            }
            )
        }
    }
    
}
