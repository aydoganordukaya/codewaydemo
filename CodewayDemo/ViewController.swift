//
//  ViewController.swift
//  CodewayDemo
//
//  Created by Aydoğan Ordukaya on 7.03.2020.
//  Copyright © 2020 Ordukaya. All rights reserved.
//

import UIKit

class ViewController: BaseViewController,UIViewControllerTransitioningDelegate {
    @IBOutlet weak var presentButton:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override var prefersStatusBarHidden: Bool { return true }

    @IBAction func presentCollection(){
        let vc = StoryCollectionViewController.createFromStoryboard(transitionFrame: presentButton.convert(presentButton.bounds, to: nil))
        let vm = StoryCollectionViewModel()
        vc.viewModel = vm
        scaleTransitionAnimator.originFrame = presentButton.convert(presentButton.bounds, to: nil)
        self.transitioningDelegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.transitioningDelegate = vc
//        vc.providesPresentationContextTransitionStyle = true
//        vc.definesPresentationContext = true
        self.present(vc, animated: true, completion: nil)
     }
    
    //MARK: - UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        scaleTransitionAnimator.presenting = true
        return scaleTransitionAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        scaleTransitionAnimator.presenting = false
        return scaleTransitionAnimator
    }
}

